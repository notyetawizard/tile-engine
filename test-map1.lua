voxel = {}

voxel.size = {
	x = 16,
	y = 10,
	z = 6
}

tempGridLevel = 1

function voxel:draw_grid(z)
	local z = z or 1
	for y = 1, 40 do
		for x = 1, 40 do
			if y % 2 == x % 2 then
				love.graphics.setColor(0, 0, 0, 63)
				love.graphics.rectangle(
					"fill",
					(x*voxel.size.x/4),
					(y*voxel.size.y/4) - (z*voxel.size.z),
					voxel.size.x/4,
					voxel.size.y/4
				)
			end
		end
	end
end

local grassBlock = {
	top = {63, 255, 63, 255},
	side = {43, 170, 43, 255}
}

local dirtBlock = {
	top = {127, 63, 63, 255},
	side = {100, 43, 43, 255}
}

local stoneBlock = {
	top = {127, 127, 127, 255},
	side = {100, 100, 100, 255}
}
	

local map = {
-- z (counts "upward")
	-- y (counts "southward")
		--x (counts "eastward")
			--data
	[1] = {
		[1] = {
			[1] = dirtBlock,
			[2] = dirtBlock,
			[3] = dirtBlock,
			[4] = dirtBlock,
			[5] = dirtBlock
		},
		[2] = {
			[1] = dirtBlock,
			[2] = dirtBlock,
			[3] = dirtBlock,
			[4] = dirtBlock,
			[5] = dirtBlock
		},
		[3] = {
			[1] = dirtBlock,
			[2] = dirtBlock,
			[3] = dirtBlock,
			[4] = dirtBlock,
			[5] = dirtBlock
		}
	},
	[2] = {
		[1] = {
			[1] = grassBlock,
			[2] = grassBlock,
			[3] = grassBlock,
			[4] = grassBlock,
			[5] = grassBlock
		},
		[2] = {
			[1] = grassBlock,
			[2] = grassBlock,
			[3] = grassBlock,
			[4] = grassBlock
		},
		[3] = {
			[1] = grassBlock,
			[2] = grassBlock,
			[3] = grassBlock
		}
	},
	[3] = {
		[1] = {
			[1] = stoneBlock,
			[2] = stoneBlock,
			[3] = stoneBlock
		},
		[2] = {
			[1] = false,
			[2] = stoneBlock
		}
	},
	[4] = {
		[1] = {
			[1] = false,
			[2] = stoneBlock
		}
	}	
}

function map:draw()
	for zk, zv in ipairs(self) do
		if zv then for yk, yv in ipairs(zv) do
			if yv then for xk, xv in ipairs(yv) do
				if xv then
					local mode = "fill"
					-- if xv == stoneBlock then mode = "line" end
					--top tile
					love.graphics.setColor(xv.top)
					love.graphics.rectangle(
						mode,
						(xk*voxel.size.x),
						(yk*voxel.size.y)-(zk*voxel.size.z),
						voxel.size.x,
						voxel.size.y
					)
					--side tile
					love.graphics.setColor(xv.side)
					love.graphics.rectangle(
						mode,
						(xk*voxel.size.x),
						((yk+1)*voxel.size.y)-(zk*voxel.size.z),
						voxel.size.x,
						voxel.size.z
					)
				end
			end end
		end end
		if zk == tempGridLevel then voxel:draw_grid(zk) end
	end
end

return map