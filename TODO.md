# Up Next
- ~~define a Voxel class, which others will be subclasses of or something.~~ Done!

- define Structure class
    - ~~An orgin point for structure, for transformations. (0, 0)~~ Done by accident.
    - Accepts a voxel map as an arguement?
    - ~~Must allow for easy empty voxels~~ Done!
    - ~~Negative xyz references, or keep everything positive? I think it's better to allow for negatives right away, so that I don't have to think about whether or not I should add them everytime someting comes up!~~ Done!
    - ~~Handy iterators to allow for the above.~~ Done!
    
- define Map class
    - Structures are applied to a Map for interaction and drawing and all that
    - Structure data is *applied* to the map; the Map doesn't contain references to the initial Structure at this point.
    - Needs a :draw() that handles objects, voxels, shading, and all that.
    
# Later
- define rotate() and translate(). They need to work on *any* voxel data; structures or maps or whatever. These are 90 degree rotations around any axis, basically reorganizing the voxel mapping—these are *not* camera functions!

- define Editor class
    - Build Structures
    - Structures can contain Structures
    - Just for editing voxels. Anything other data should be added as code when the structure is initilized.
    - Exports a pretty lua table into a text file. I built a serializer for this a while ago :)

- define Camera()? I'm not sure where this should go, yet, but it needs to go somewhere. This should handle issues of drawing, including rotating the view around the x axis to go from top-down incrementally to side-view, and 90 degree rotations around the z axis. Rotation around the y axis is okay when 90 degrees, or incrementally when x is at a 90 degree angle ... but that doesn't seem useful, does it?
    - I suppose tile sizes should be defined here too!

- define voxel.Generator class
    - builds Structures or Maps from rules
    
- define Objects class
- "False" physics for Objects interacting with Structures
- "True" physics for Objects interacting with Objects

# Way Later
- Example game. I think a zelda-like with spiral knights elements? ;)

- Port everything off LOVE, probably onto C + SDL2