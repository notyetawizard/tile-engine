-- Some useful local assertions, not passed on through the libary
local function assertXYZ(x, y, z)
	assert(
		type(x) == "number"
		and type(y) == "number"
		and type(z) == "number",
		"Numeric x, y, and z values are mandatory!"
	)
end

local function assertColor(color)
	assert(
		type(color) == "table"
		and  0 >= color[1] <= 255
		and  0 >= color[2] <= 255
		and  0 >= color[3] <= 255
		and  (not color[4] or 0 >= color[4] <= 255),
		"This needs an {r, g, b, a} color table to pass!"
	)
end

-- Voxmap class
local Voxmap = {}

function Voxmap:exists(x, y, z, create)
	assert(getmetatable(self) == Voxmap, "This is not a Voxmap object!")
	assertXYZ(x, y, z)

	if create == true then
		if not self.grid[x] then
			self.grid[x] = {[y] = {[z] = {}}}
		elseif not self.grid[x][y] then
			self.grid[x][y] = {[z] = {}}
		elseif not self.grid[x][y][z] then
			self.grid[x][y][z] = {}
		end
		
		if x < self.size.x.min then self.size.x.min = x
		elseif x > self.size.x.max then self.size.x.max = x
		end
		
		if y < self.size.y.min then self.size.y.min = y
		elseif y > self.size.y.max then self.size.y.max = y
		end
		
		if z < self.size.z.min then self.size.z.min = z
		elseif z > self.size.z.max then self.size.z.max = z
		end

		return true
	elseif self.grid[x] and self.grid[x][y] and self.grid[x][y][z] then
		return true
	else
		return false
	end
end

function Voxmap:get(x, y, z)
	assert(getmetatable(self) == Voxmap, "This is not a Voxmap object!")
	-- assertXYZ() in exists()
	if self:exists(x, y, z) then
		return self.grid[x][y][z]
	else
		return false
	end
end

function Voxmap:set(x, y, z, data)
	assert(getmetatable(self) == Voxmap, "This is not a Voxmap object!")
	-- assertXYZ() in exists()
	assert(type(data) == "table", "Some data is needed to create a voxel!")
	self:exists(x, y, z, true)
	for k, v in pairs(data) do
		self.grid[x][y][z][k] = v
	end
end

function Voxmap:del(x, y, z)
	assert(getmetatable(self) == Voxmap, "This is not a Voxmap object!")
	--assertXYZ() in exists()
	if self:exists(x, y, z) then
		self.grid[x][y][z] = nil
	end
end

function Voxmap:iterate()
	assert(getmetatable(self) == Voxmap, "This is not a Voxmap object!")
	-- needs to be able to reverse axes too, I think. Not until I add rotation, though.
	local state = self.grid
	local control = {
		x = self.size.x.min - 1,
		y = self.size.y.min, 
		z = self.size.z.min,
	}
		
	local function iterator(state, control)
		while true do
			control.x = control.x + 1
			if control.x > self.size.x.max then
				control.y = control.y + 1
				if control.y > self.size.y.max then
					control.z = control.z + 1
					if control.z > self.size.z.max then
						break
					end
					control.y = self.size.y.min
				end
				control.x = self.size.x.min
			end
			
			if self:exists(control.x, control.y, control.z) then
				value = state[control.x][control.y][control.z]
				return control, value
			end
		end
		
		return nil
	end

	return iterator, state, control
end

function Voxmap:new()
	local new = {
		grid = {
			[0] = { -- x
				[0] = { -- y
					[0] = {} -- z
				}
			}
		},
		size = {
			x = {min = 0, max = 0},
			y = {min = 0, max = 0},
			z = {min = 0, max = 0}
		}
	}

	for k, v in pairs(self) do
		if type(v) == "function" and k ~= "new" then
			new[k] = v
		end
	end

	setmetatable(new, Voxmap)
	return new
end

return Voxmap