-- CURRENTLY JUNK!
-- A new *global* iterator, because why not!
-- I don't think this can be useful locally, but it's not even used here. Should it go somewhere else?
function ivoxels(slice, size)
	assert(type(slice) == "table", "This should be a slice from a Voxel.grid matrix")
	assert(size.min and size.max, "You'll want to pass this an entry from Voxel.size.")
	local control = size.min - 1
	
	-- return, in order:
		-- an iterator function
		-- the "invarient state"; the table in pairs(table)!
		-- control state; the i of things, as it were

	-- for will then pass the iterator the invariant state the i until i == nil
	-- which must return a new i, and can return more!

	local function iterator(state, control)
		control = control + 1
		local value = state[control]
		while value == nil and control <= size.max do
			control = control + 1
			value = state[control]	
		end
		if control <= size.max then
			return control, value
		else
			return nil
		end
	end

	return iterator, slice, control
end

-- test the iterator
local testSlice = {
	[-4] = 1,
	[-2] = 2,
	[-1] = 3,
	[3] = 4,
	[5] = 5
}

for k, v in ivoxels(testSlice, {min=-4, max=6}) do
	print(k, v)
end