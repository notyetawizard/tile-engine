local voxmap = require "voxmap"

-- testing the voxel stuff :)

local grassBlock = {
	color = {63, 255, 63, 255}
}

local dirtBlock = {
	color = {127, 63, 63, 255}
}

local stoneBlock = {
	color = {127, 127, 127, 255}
}

map = voxmap:new()
for y = -2, 3 do
	for x = -5, 10 do
		map:set(x, y, 0, dirtBlock)
	end
end

for y = -2, 2 do
	for x = -3, 7 do
		map:set(x, y, 1, grassBlock)
	end
end

map:set(8, -2, 1, grassBlock)

for k, v in map:iterate() do
	if math.random(1, 10) == 4 then
		map:del(k.x, k.y, k.z)
	end
end

for k, v in map:iterate() do
	local color = {}
	color[1] = v.color[1] + math.random(-8, 8)
	color[2] = v.color[2] + math.random(-8, 8)
	color[3] = v.color[3] + math.random(-8, 8)
	v.color = color
end

local size = {
	x = 16,
	y = 10,
	z = 6
}

local function shade(color)
	ncolor = {}
	for k = 1, 3 do
		if color[k] > 64 then
			ncolor[k] = color[k] - 64
		else
			ncolor[k] = 0
		end
	end
	ncolor[4] = color[4]
	return ncolor
end

function map:draw()
	for k, v in self:iterate() do
		local mode = "fill"
		-- if xv == stoneBlock then mode = "line" end
		--top tile
		love.graphics.setColor(v.color)
		love.graphics.rectangle(
		mode,
		(k.x*size.x),
		(k.y*size.y)-(k.z*size.z),
		size.x,
		size.y
		)
		--side tile
		love.graphics.setColor(shade(v.color))
		love.graphics.rectangle(
		mode,
		(k.x*size.x),
		((k.y+1)*size.y)-(k.z*size.z),
		size.x,
		size.z
		)
	end
end

function love.load()
	love.keyboard.setKeyRepeat(true)
end

function love.keypressed(key)
	-- note that this is *only* acceptable for testing.
	-- it's crap for real rotation.
	if key == "up" then
		size.y = size.y-1 
		size.z = size.z+1
	elseif key == "down" then
		size.y = size.y+1 
		size.z = size.z-1
	--[[
	elseif key == "pageup" then
		tempGridLevel = tempGridLevel+1
	elseif key == "pagedown" then
		tempGridLevel = tempGridLevel-1
	--]]
	end
end

function love.update()

end

function love.draw()
	love.graphics.scale(2, 2)
	love.graphics.translate(128, 128)
	map:draw()
end